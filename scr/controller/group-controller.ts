import {BaseController} from "./base-controller";
import {Request, Response} from "express";

export class GroupController extends BaseController {
    async create(req: Request, res: Response) {
        const {name, surname} = req.body
        console.log(name, surname)
        res.json('Group Controller is OK')
    }

    async getAll(req: Request, res: Response) {

    }

    async getById(req: Request, res: Response) {

    }

    async update(req: Request, res: Response) {

    }

    async delete(req: Request, res: Response) {

    }

}
